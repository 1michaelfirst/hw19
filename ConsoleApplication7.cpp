﻿#include <iostream>

class Animal {
public:
    virtual void Voice() = 0;
};

class Dog : public Animal
{
public:
    void Voice() override {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override {
        std::cout << "Meow!" << std::endl;
    }
};

class Cow : public Animal
{
public:
    void Voice() override {
        std::cout << "Moo!" << std::endl;
    }
};

int main()
{
    const int ANIMALS_COUNT = 3;
    Animal** animals = new Animal * [ANIMALS_COUNT] {new Dog(), new Cat(), new Cow()};

    for (int i = 0; i < ANIMALS_COUNT; i++) {
        auto animal = animals[i];
        animal->Voice();
    }

    for (int i = 0; i < ANIMALS_COUNT; i++) {
        delete animals[i];
    }

    delete[] animals;
}
